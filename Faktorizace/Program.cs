﻿using System;

namespace Faktorizace
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var input = 446342601787;
            
            for (var iterator = 2; input > 1; iterator++) {
                if (input % iterator == 0)
                {
                    int foundResults = 0;
                    while (input % iterator == 0)
                    {
                        input /= iterator;
                        foundResults++;
                    }
                    Console.WriteLine("Prvočíslo {0} se objevilo {1} krát!", iterator, foundResults);
                }
            }
            
            Console.WriteLine("Kalkulace dokončena!");
        }
    }
}